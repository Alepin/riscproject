#ifndef  CONTROL_UNIT_HPP
#define CONTROL_UNIT_HPP

#include <systemc.h>

SC_MODULE(ControlUnit){


	//Clock e reset
	sc_in<bool> clk;
	sc_in<bool> reset;
	
	// segnali di controllo dalla control unit
	sc_out<sc_logic> loadPc;
	sc_out<sc_logic> loadIr;
	sc_out<sc_logic> loadSrc1;
	sc_out<sc_logic> loadSrc2;
	sc_out<sc_logic> loadAluout;
	sc_out<sc_logic> loadImm;
	sc_out<sc_logic> loadImm10;
	sc_out<sc_logic> loadMdr;
	sc_out<sc_logic> loadNewPc;
	sc_out<sc_logic> loadJump;
	sc_out<sc_logic> selMuxOp2Alu;
	sc_out<sc_logic> selMuxSrc2RegFile;
	sc_out<sc_lv<2> > selMuxPc;
	sc_out<sc_lv<2> > selMuxWriteRegFile;
	sc_out<sc_lv<2> > selAluOp;
	sc_out<sc_logic> weRegFile;
	sc_out<sc_logic> weDataMem;
	sc_out<sc_logic> readDataMem;
	
	//ingressi
	sc_in <sc_logic> isZero;
	sc_in <sc_lv<3> > instToControlUnit;
	
	
	enum StateEnum { start, fetch, decode, aluexecution, writeback, jump, beqstate, memaccessread, memaccesswrite};
	//funzione per testing
	void print_state();
	
	StateEnum state,nextState; 
	
	sc_event changeState_event;
	
	SC_CTOR( ControlUnit){
		
		init();
		
		SC_METHOD( changeState_method)
			sensitive << clk.pos() << reset;
		
		
		SC_METHOD(control_unit_method);
			sensitive << instToControlUnit << changeState_event << isZero;
			dont_initialize();
	}
	
	private: 
	void changeState_method();
	void control_unit_method();
	void init();
};


#endif