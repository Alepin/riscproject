#ifndef  REGISTER_FILE_HPP 
#define REGISTER_FILE_HPP

#include <systemc.h>
#define N_BIT 16
#define SIZE_MEM 8

/*
Register file a sedici bit  e 8 registri il primo registro sarà sempre 0 e non può essere modificato
ha: due ingressi per gli indirizzi di lettura, un ingresso per l'indirizzo di scrittura, due uscite dati per leggere 
il contenuto dei registri, un ingresso dati per la scrittura nel registro e un valore di write enable in ingresso per abilitare
la scrittura
*/ 

SC_MODULE(Register_file){
	//ingressi
	sc_in<sc_lv<3> > address1;
	sc_in<sc_lv<3> > address2;
	sc_in<sc_lv<3> > address_write;
	sc_in<sc_lv<N_BIT> > data_write;
	sc_in<bool > clk;
	sc_in<sc_logic> we;
	sc_in<bool> reset;
	
	//usicte
	sc_out<sc_lv<N_BIT> > dataout1;
	sc_out<sc_lv<N_BIT> > dataout2;
	
	
	//funzione che ritorna un valore del registro usata per il testing
	int getRegisterValue( int address);
	
	SC_CTOR(Register_file){
		
		for(int i =0; i<SIZE_MEM ; i++)
			RegMem[i] = 0;
		
		SC_METHOD(regfile_method);
			sensitive << clk.pos() << we  << reset << address1 << address2;
				dont_initialize();
	}
	
	private:
	//Memoria
	sc_lv <N_BIT> RegMem[SIZE_MEM];
	void regfile_method();
	void writeRegFile();
	void readRegFile();
	
};

#endif