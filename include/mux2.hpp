#ifndef MUX_2
#define MUX_2

#include <systemc.h>
#define N_BIT 16

//Modulo multiplexer con 2 ingressi

SC_MODULE (Mux2){
	
	sc_in<sc_lv<N_BIT> > data_1, data_2;
	sc_in< sc_logic > sel_in;
	sc_out<sc_lv<N_BIT> > out;
	
	SC_CTOR(Mux2){
		SC_METHOD(mux_method);
			sensitive << data_1 << data_2 << sel_in;
			dont_initialize();
	}
	
	private:
	
	void mux_method();
	
};

#endif