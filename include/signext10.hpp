#ifndef  SIGN_EXT10_HPP
#define SIGN_EXT10_HPP

#include <systemc.h>
#define N_BIT 16
#define BIT_TEN 10

/* Modulo che fa l'estesione del segno se il segno se il bit più significativo è uno agginge uno se il bit meno significativo è 
0 agiunge zeri modulo non sensibile al clock fa sempre l'estensione di segno dell'ingresso 
*/

SC_MODULE(Sign_extend10){
	
	// ingressi 
	sc_in<sc_lv<BIT_TEN> > in10;
	
	//uscite
	sc_out <sc_lv< N_BIT> > signext10;
	
	SC_CTOR(Sign_extend10){
		
		SC_METHOD(extend_method10);
			sensitive << in10;
			dont_initialize();
	}
	
	private:
	void extend_method10();
	
};

#endif