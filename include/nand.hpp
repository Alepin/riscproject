#ifndef NAND_HPP
#define	NAND_HPP

#include <systemc.h>
#define N_BIT 16

/*fa operazione di NAND bia a bit dei due segnali d'ingresso*/

SC_MODULE(Nand){
	sc_in<sc_lv<N_BIT> > op1, op2;
	sc_out <sc_lv < N_BIT> > res;
	
	SC_CTOR(Nand){
		SC_THREAD(nand_thread);
			sensitive << op1 << op2;
	}
	
	private:
	void nand_thread();
	
};

#endif