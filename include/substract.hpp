#ifndef 	SUBSTRACT_HPP
#define	SUBSTRACT_HPP 

#include <systemc.h>
#define N_BIT 16

// modulo che fa la sottrazione per implementare un comparatore più strutturale

SC_MODULE(Substract){
	sc_in<sc_lv<N_BIT> > op1, op2;
	sc_out< sc_lv<N_BIT> > res_sub;
	
	SC_CTOR(Substract){
		SC_METHOD(sub_method);
		sensitive << op1 << op2;
		dont_initialize();
	}
	
	private:
	
	void sub_method();
	
};

#endif