#ifndef MUX_4
#define MUX_4

#include <systemc.h>
#define N_BIT 16

//Modulo multiplexer con 4 ingressi

SC_MODULE (Mux4){
	
	sc_in<sc_lv<N_BIT> > data_1, data_2, data_3, data_4;
	sc_in< sc_lv<2> > sel_in;
	sc_out<sc_lv<N_BIT> > out;
	
	SC_CTOR(Mux4){
		SC_METHOD(mux_method);
			sensitive << data_1 << data_2 << data_3 << sel_in << data_4;
			dont_initialize();
	}
	
	private:
	
	void mux_method();
};

#endif