#ifndef ALU_HPP
#define ALU_HPP
#include <systemc.h>
#include "beq.hpp"
#include "adder.hpp"
#include "mux3.hpp"
#include "nand.hpp"
#include "oppass.hpp"
//#include "substract.hpp"
//#include"eqzero.hpp"
#define N_BIT 16
#define N_ALU_OP 2

/*implementazione di una alu 16 bit con operazioni di add nand comparazione e un modulo che lascia passare 
inaltarato il primo ingrasso della alu.
La ALU ha 2 operatori in ingresso e un uscita da 16 bit e un ingresso a 2 bit per la selezione dell'operazione 
 e una uscita logica da un solo bit che da il risultato della comparazione */

SC_MODULE(Alu){
	
	// ingressi
	sc_in <sc_lv<N_BIT> > opalu1, opalu2;
	sc_in<sc_lv<N_ALU_OP> > selopalu;
	
	//uscite
	sc_out <sc_lv<N_BIT> > res_alu;
	sc_out <sc_logic> equal;
	
	//segnali 
	sc_signal<sc_lv<N_BIT> > nand_out;
	sc_signal<sc_lv<N_BIT> > add_out;
	sc_signal<sc_lv<N_BIT> > oppass_out;
	//oggetti rappresentanti i componenti
	Beq comparator;
	Adder add;
	Mux3 mux;
	Nand nand;
	Op_pass pass;
	
	SC_CTOR(Alu) : comparator("comparator"), add("add"), mux("mux"), nand("nand"), pass("pass"){
		//collegamento comparatore
		comparator.op1(this-> opalu1);
		comparator.op2(this-> opalu2);
		comparator.is_eq(this -> equal);
		//collegamento adder
		add.op1 (this ->opalu1);
		add.op2(this -> opalu2);
		add.res(this -> add_out);
		//collegamento nand
		nand.op1(this -> opalu1);
		nand.op2(this -> opalu2);
		nand.res(this -> nand_out);
		//collegamento op_pass
		pass.op1(this -> opalu1);
		pass.op2(this -> opalu2);
		pass.res(this -> oppass_out);
		//collegamento mux
		mux.data_1(this -> add_out);
		mux.data_2(this -> nand_out);
		mux.data_3(this -> oppass_out);
		mux.sel_in(this -> selopalu);
		mux.out(this -> res_alu);
		
	}
	
	
};

#endif