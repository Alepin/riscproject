#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <systemc.h>
#define N_BIT 16

/*
registro sensibile al clock 
*/

SC_MODULE (Reg){
	
	sc_in<bool> clk;
	sc_in<bool> reset;
	sc_in<sc_logic> load;
	sc_in<sc_lv<N_BIT> > datain;
	sc_out < sc_lv <N_BIT> > dataout;
	
	//metodo per testing
	int getRegValue();
	
	SC_CTOR(Reg){
		
		SC_METHOD(reg_method);
			sensitive << clk.pos() << reset << load << datain;
			dont_initialize();
			
	}
	
	private: 
	sc_lv<N_BIT> value;
	void reg_method();
	
};

#endif