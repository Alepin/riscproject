#ifndef DATAPATH_HPP
#define DATAPATH_HPP

#include <systemc.h>
#include "datamem.hpp"
#include "signext.hpp"
#include "mux2.hpp"
#include "mux2_3bit.hpp"
#include "mux3.hpp"
#include "mux4.hpp"
#include "adder.hpp"
#include "alu.hpp"
#include "instrmem.hpp"
#include "regfile.hpp"
#include "register.hpp"
#include "signext10.hpp"

/* implementazione del datapath con tutti i componenti verra testato assieme alla control unit*/

SC_MODULE(Datapath){
	
	//Clock e reset
	sc_in<bool> clk;
	sc_in<bool> reset;
	
	// ingressi	e segnali di controllo dalla control unit
	sc_in<sc_logic> loadPc;
	sc_in<sc_logic> loadIr;
	sc_in<sc_logic> loadSrc1;
	sc_in<sc_logic> loadSrc2;
	sc_in<sc_logic> loadAluout;
	sc_in<sc_logic> loadImm;
	sc_in<sc_logic> loadImm10;
	sc_in<sc_logic> loadMdr;
	sc_in<sc_logic> loadNewPc;
	sc_in<sc_logic> loadJump;
	sc_in<sc_logic> selMuxOp2Alu;
	sc_in<sc_logic> selMuxSrc2RegFile;
	sc_in<sc_lv<2> > selMuxPc;
	sc_in<sc_lv<2> > selMuxWriteRegFile;
	sc_in<sc_lv<2> > selAluOp;
	sc_in<sc_logic> weRegFile;
	sc_in<sc_logic> weDataMem;
	sc_in<sc_logic> readDataMem;
	
	//uscite
	sc_out <sc_logic> isZero;
	sc_out <sc_lv<3> > instToControlUnit;
	
	//segnali interni per collegare i moduli
	sc_signal< sc_lv<N_BIT> > one;
	sc_signal< sc_lv<N_BIT> > aluOut;
	sc_signal< sc_lv<N_BIT> > memOut;
	sc_signal< sc_lv<N_BIT> > irOut;
	sc_signal< sc_lv<N_BIT> > ext7Out;
	sc_signal< sc_lv<N_BIT> > ext10Out;
	sc_signal< sc_lv<N_BIT> > src2Out; 
	sc_signal< sc_lv<N_BIT> > immOut;
	sc_signal< sc_lv<N_BIT> > op2Alu;
	sc_signal< sc_lv<N_BIT> > newPcOut;
	sc_signal< sc_lv<N_BIT> > jumpRegOut;
	sc_signal< sc_lv<N_BIT> > nextInstrPc;
	sc_signal< sc_lv<N_BIT> > mdrOut;
	sc_signal< sc_lv<N_BIT> > dataWriteReg;
	sc_signal< sc_lv<N_BIT> > imm10Out;
	sc_signal< sc_lv<N_BIT> > addPc1Out;
	sc_signal< sc_lv<N_BIT> > PCout;
	sc_signal< sc_lv<N_BIT> > addPcJumpOut; 
	sc_signal< sc_lv<N_BIT> > src1Out;
	sc_signal< sc_lv<N_BIT> > resAlu;
	sc_signal< sc_lv<N_BIT> > instruction;
	sc_signal< sc_lv<N_BIT> > data1;
	sc_signal< sc_lv<N_BIT> > data2;

	sc_signal< sc_lv<TREBIT> > regFileSrc2;
	sc_signal< sc_lv<10> > ext10in;
	sc_signal< sc_lv<7> > ext7in;
	sc_signal< sc_lv<TREBIT> > src2first;
	sc_signal< sc_lv<TREBIT> > src2second;
	sc_signal< sc_lv<TREBIT> > src1fromIr;
	sc_signal< sc_lv<TREBIT> > tgtfromIr;
	
	//Componenti
	//Ram
	Data_memory Ram;
	//Estensione di segno
	Sign_extend ext7;
	Sign_extend10 ext10;
	//Multiplexer vari
	Mux2_3bit src2Mux;
	Mux2 op2AluMux;
	Mux3 newPcMux;
	Mux4 writeRegFileMux;
	//Adder per calcolo PC+1 e per calcolo PC in caso di jump
	Adder addPc1;
	Adder addPcJump;
	//ALU
	Alu aluUnit;
	//Instruction memory
	Instruction_memory instrMem;
	//Register File
	Register_file regFile;
	//Registri vari 
	Reg PC;
	Reg newPc;
	Reg jumpReg;
	Reg iReg;
	Reg imm;
	Reg imm10;
	Reg src1;
	Reg src2;
	Reg aluOutReg;
	Reg mdr;
	
	//per testing
	void printRegisterFile();
	void getRegisterFile( int vector[] );
	void printMemValue ( int address);
	int getMemValue (int address);
	void getRegValue(int vector[]);
	void printRegValue();
	
	SC_CTOR(Datapath)  : Ram("Ram"),  ext7("ext7"),  ext10("ext10"), src2Mux("src2Mux"), op2AluMux("op2AluMux"),
	newPcMux("newPcMux"), writeRegFileMux("writeRegFileMux"), addPc1("addPc1"), addPcJump("addPcJump"),
	aluUnit("aluUnit"), instrMem("instrMem"), regFile("regFile"), PC("PC"), newPc("newPc"), jumpReg("jumpReg"),
	 iReg("iReg"), imm("imm"), imm10("imm10"), src1("src1"), src2("src2"), aluOutReg("aluOutReg"), mdr("mdr") {
		
		one.write(1);
		SC_METHOD(datapath_method);
			sensitive << reset << loadPc << loadIr <<	loadSrc1 << loadSrc2 << loadAluout << loadImm << loadImm10 
			<< loadMdr << loadNewPc << loadJump << selMuxOp2Alu << selMuxSrc2RegFile << selMuxPc << selMuxWriteRegFile 
			<< selAluOp << weRegFile << weDataMem << readDataMem;		// togliere sensibilità al clock pos
			
		//collegamento ram
		Ram.address (this -> aluOut);
		Ram.datain (this -> src2Out);
		Ram.dataout (this -> memOut);
		Ram.wedatamem (this -> weDataMem);
		Ram.readmem (this -> readDataMem);
		Ram.reset (this -> reset);
		Ram.clk (this -> clk );
		
		// collegamento moduli estensione di segno
		ext7.in7 ( this -> ext7in);
		ext7.signext ( this -> ext7Out);
		
		ext10.in10( this -> ext10in);
		ext10.signext10( this -> ext10Out);
		
		//Collegamento mux
		src2Mux.data_1 ( this -> src2first);
		src2Mux.data_2 ( this -> src2second);
		src2Mux.sel_in 	( this -> selMuxSrc2RegFile);
		src2Mux.out 		( this -> regFileSrc2);
		
		op2AluMux.data_1 (this -> src2Out );
		op2AluMux.data_2 (this -> immOut);
		op2AluMux.sel_in 	(this -> selMuxOp2Alu);
		op2AluMux.out 		(this -> op2Alu);
		
		newPcMux.data_1  ( this -> newPcOut);
		newPcMux.data_2  ( this -> jumpRegOut);
		newPcMux.data_3 	( this -> aluOut);
		newPcMux.sel_in 	( this -> selMuxPc); 		
		newPcMux.out 		( this -> nextInstrPc);
		
		writeRegFileMux.data_1 	( this -> newPcOut);
		writeRegFileMux.data_2 	( this -> aluOut);
		writeRegFileMux.data_3 	( this -> mdrOut);
		writeRegFileMux.data_4 	( this -> imm10Out );
		writeRegFileMux.sel_in  	( this -> selMuxWriteRegFile);
		writeRegFileMux.out 	   	( this -> dataWriteReg);
		
		//collegamento adder
		addPc1.op1( this -> PCout);
		addPc1.op2( this -> one );
		addPc1.res ( this -> addPc1Out);
		
		addPcJump.op1( this -> newPcOut);
		addPcJump.op2( this -> ext7Out);
		addPcJump.res( this -> addPcJumpOut);
		
		//collegamento ALU
		aluUnit.opalu1( this-> src1Out);
		aluUnit.opalu2( this-> op2Alu);
		aluUnit.selopalu( this-> selAluOp);
		aluUnit.res_alu( this-> resAlu);
		aluUnit.equal( this-> isZero);
		
		//collegamento Instruction Memory
		instrMem.pc ( this -> PCout);
		instrMem.reset ( this -> reset);
		instrMem.clk ( this -> clk);
		instrMem.instruction ( this -> instruction);
		
		//collegamentto register file
		regFile.address1( this -> src1fromIr);
		regFile.address2( this -> regFileSrc2);
		regFile.address_write ( this -> tgtfromIr);
		regFile.data_write ( this -> dataWriteReg);
		regFile.clk( this -> clk);
		regFile.we( this -> weRegFile);
		regFile.reset( this -> reset);
		regFile.dataout1( this -> data1);
		regFile.dataout2( this -> data2);
		
		//collegamento dei registri 
		PC.datain 		( this -> nextInstrPc);
		PC.dataout 	( this -> PCout);
		PC.load 		( this -> loadPc);
		PC.reset 		( this -> reset);
		PC.clk 			( this -> clk);
		
		newPc.datain 	( this -> addPc1Out );
		newPc.dataout 	( this -> newPcOut);
		newPc.load 		( this -> loadNewPc );
		newPc.reset 		( this -> reset );
		newPc.clk 		( this -> clk );
		
		jumpReg.datain 	 ( this -> addPcJumpOut);
		jumpReg.dataout ( this -> jumpRegOut);
		jumpReg.load 	 ( this -> loadJump);
		jumpReg.reset 	 ( this -> reset );
		jumpReg.clk 		 ( this -> clk);
		
		iReg.datain 	 ( this -> instruction);
		iReg.dataout ( this -> irOut);
		iReg.load 	 ( this -> loadIr);
		iReg.reset 	 ( this -> reset );
		iReg.clk 		 ( this -> clk);
		
		imm.datain 	 ( this -> ext7Out);
		imm.dataout ( this -> immOut);
		imm.load 	 ( this -> loadImm);
		imm.reset 	 ( this -> reset );
		imm.clk 		 ( this -> clk);
		
		imm10.datain 	 ( this -> ext10Out);
		imm10.dataout ( this -> imm10Out);
		imm10.load 	 ( this -> loadImm10);
		imm10.reset 	 ( this -> reset );
		imm10.clk 		 ( this -> clk);
		
		src1.datain 	 ( this -> data1);
		src1.dataout ( this -> src1Out);
		src1.load 	 ( this -> loadSrc1);
		src1.reset 	 ( this -> reset );
		src1.clk 		 ( this -> clk);
		
		src2.datain 	 ( this -> data2);
		src2.dataout ( this -> src2Out);
		src2.load 	 ( this -> loadSrc2);
		src2.reset 	 ( this -> reset );
		src2.clk 		 ( this -> clk);
		
		aluOutReg.datain 	 ( this -> resAlu);
		aluOutReg.dataout ( this -> aluOut);
		aluOutReg.load 	 ( this -> loadAluout);
		aluOutReg.reset 	 ( this -> reset );
		aluOutReg.clk 		 ( this -> clk);
		
		mdr.datain 	 ( this -> memOut);
		mdr.dataout ( this -> mdrOut);
		mdr.load 	 ( this -> loadMdr);
		mdr.reset 	 ( this -> reset );
		mdr.clk 		 ( this -> clk);

	}
	
	private:
	void datapath_method();
	
};

#endif