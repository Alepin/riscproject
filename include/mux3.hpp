#ifndef MUX_3
#define MUX_3

#include <systemc.h>
#define N_BIT 16

//Modulo multiplexer con 3 ingressi

SC_MODULE (Mux3){
	
	sc_in<sc_lv<N_BIT> > data_1, data_2, data_3;
	sc_in< sc_lv<2> > sel_in;
	sc_out<sc_lv<N_BIT> > out;
	
	SC_CTOR(Mux3){
		SC_METHOD(mux_method);
			sensitive << data_1 << data_2 << data_3 << sel_in;
			dont_initialize();
	}
	
	private:
	
	void mux_method();
};

#endif