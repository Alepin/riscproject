#ifndef EQ_ZERO_HPP
#define EQ_ZERO_HPP

#include <systemc.h>
#define N_BIT 16

/*
componente che fa un OR su tutti i bit di ingresso per vedere se il numero in ingresso è zero
*/

SC_MODULE(Eq_zero){
	sc_in<sc_lv<N_BIT> > ingresso;
	sc_out< sc_logic> is_zero;
	
	SC_CTOR(Eq_zero){
		
		SC_METHOD(zero_method);
			sensitive << ingresso;
		dont_initialize();
	}
	
	private:
	
	void zero_method();
	
};

#endif