#ifndef BEQ_HPP
#define BEQ_HP
#include <systemc.h>
#include "eqzero.hpp"
#include "substract.hpp"
#define N_BIT 16

/*
fa la sottrazione degli ingressi e pone il bit in uscità a 1 se la sottazione 
risulta 0 cioè se i due ingressi sono uguali altrimenti ritorna 0
*/
SC_MODULE (Beq){
	
	
	sc_in <sc_lv<N_BIT> > op1, op2;
	sc_out <sc_logic> is_eq;
	sc_signal<sc_lv<N_BIT> > subout;
	
	Substract sub;
	Eq_zero ezero;

	SC_CTOR(Beq) : sub("sub") , ezero("ezero") {
		sub.op1(this->op1);
		sub.op2(this->op2);
		sub.res_sub(this->subout);
		ezero.ingresso(this->subout);
		ezero.is_zero(this->is_eq);
	}
/*
	private:
		
		void beq_method();
*/
};

#endif
