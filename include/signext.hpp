#ifndef SIGN_EXT_HPP
#define SIGN_EXT_HPP

#include <systemc.h>
#define N_BIT 16
#define BIT_IN 7

/* Modulo che fa l'estesione del segno se il segno se il bit più significativo è uno agginge uno se il bit meno significativo è 
0 agiunge zeri modulo non sensibile al clock fa sempre l'estensione di segno dell'ingresso
*/

SC_MODULE(Sign_extend){
	
	// ingressi 
	sc_in<sc_lv<BIT_IN> > in7;
	
	//uscite
	sc_out <sc_lv< N_BIT> > signext;
	
	SC_CTOR(Sign_extend){
		
		SC_METHOD(extend_method);
			sensitive << in7;
			dont_initialize();
	}
	
	private:
	void extend_method();
	
};

#endif