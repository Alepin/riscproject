#ifndef Adder_HPP
#define Adder_HPP

#include <systemc.h>
#define N_BIT 16
using namespace sc_core;

SC_MODULE(Adder){

	sc_in< sc_lv<N_BIT> > op1;
	sc_in< sc_lv<N_BIT> > op2;
	sc_out < sc_lv<N_BIT> > res;

	SC_CTOR(Adder){
			SC_THREAD(add);
				sensitive << op1 << op2;
	}

	private:
	void add();

};

#endif
