#ifndef  OPERATOR_PASS_HPP
#define OPERATOR_PASS_HPP

#include <systemc.h>
#define N_BIT 16

//riceve in ingresso due operatori e da in uscita il primo operatore invariato

SC_MODULE (Op_pass){
	
	sc_in<sc_lv<N_BIT> > op1, op2;
	sc_out<sc_lv<N_BIT> > res;
	
	SC_CTOR(Op_pass){
		SC_METHOD(pass_method);
			sensitive << op1 << op2;
			dont_initialize();
	}
	
	private:
	void pass_method();
	
};


#endif 
 