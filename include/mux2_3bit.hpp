#ifndef MUX_2_3bit
#define MUX_2_3bit

#include <systemc.h>
#define TREBIT 3

//Modulo multiplexer con 2 ingressi

SC_MODULE (Mux2_3bit){
	
	sc_in<sc_lv<TREBIT> > data_1, data_2;
	sc_in< sc_logic > sel_in;
	sc_out<sc_lv<TREBIT> > out;
	
	SC_CTOR(Mux2_3bit){
		SC_METHOD(mux_method);
			sensitive << data_1 << data_2 << sel_in;
			dont_initialize();
	}
	
	private:
	
	void mux_method();
	
};

#endif