#ifndef  INSTRUCTION_MEMORY_HPP 
#define INSTRUCTION_MEMORY_HPP

#include <systemc.h>
#define N_BIT 16
#define BIT_INSTR_MEM 8
#define SIZE_INSTR_MEM 512 

/*Instruction memory ha celle di memoria da 8 bit l'una e un'istruzione e formata dal contenuto di due celle 
adiacenti in ingresso ha un valore a 16-bit che indica l'istruzione da eseguire, questo ingresso viene dato
dal programm counter l'uscita è l'istruzione a 16-bit, è sincronizzato con il clock.
In questo modulo precarico gia delle istruzioni che comprendono le istruzioni le 8 istruzioni del mio sistema cioe 
add, addi , lui, lw, sw, nand, beq e jarl
L'attivazione del reset porta l'uscita a "0000000000000000" non cancella la memoria
*/

SC_MODULE(Instruction_memory){
	//ingressi
	sc_in<sc_lv<N_BIT> > pc;
	sc_in<bool> reset;
	sc_in<bool> clk;
	
	//uscita
	sc_out<sc_lv<N_BIT> > instruction;
	
	//funzione per testing mi ritorna l'istruzione in decimale
	sc_lv<N_BIT> getInstr(int pcval);
	
	SC_CTOR(Instruction_memory){
		//funzione che carica le istruzioni in memoria
		init();
		
		SC_METHOD(Instruction_method)
			sensitive << clk << reset << pc;
			
	}
	
	
	private:
	//memoria
	sc_lv<BIT_INSTR_MEM> InstrMem[SIZE_INSTR_MEM];
	
	void init();
	void Instruction_method();
	void read_instr();
	
};

#endif