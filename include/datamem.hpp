#ifndef DATA_MEM_HPP
#define DATA_MEM_HPP

#include <systemc.h>
#define N_BIT 16
#define SIZE_DATA_MEM 65535 

/*
Memoria dati esterna con indirizzi a 16 bit  ha 2 ingressi a 16 bit uno per l'indirizzo e uno per i dati 
un wrtie enable un segnale di read un reset e clock per fare scritture sincrone ha un uscita a 16-bit per i dati
*/

SC_MODULE (Data_memory){
	// ingressi
	sc_in<sc_lv<N_BIT> > address;
	sc_in<sc_lv<N_BIT> > datain;
	sc_in<bool> clk;
	sc_in<sc_logic> wedatamem;
	sc_in<sc_logic> readmem;
	sc_in<bool> reset;
	//uscite
	sc_out<sc_lv<N_BIT> > dataout;
	
	//funzione che ritorna il contenuto della memoria per testing
	int getMemValue( unsigned add);
	
	SC_CTOR(Data_memory){
		
		for(int i=0; i<SIZE_DATA_MEM ; i++)
			DataMem[i] = 0;
		
		SC_METHOD(datamem_method)
			sensitive << reset << readmem << wedatamem << reset;
				dont_initialize();
	}
	
	private:
	//Memoria 
	sc_lv<N_BIT> DataMem[SIZE_DATA_MEM];
	
	void datamem_method();
	void writeDataMem();
	void readDataMem();
	
};

#endif