#include <systemc.h>
#include "substract.hpp"

void Substract::sub_method(){
	
	res_sub->write( op1->read().to_int() - op2->read().to_int());
	next_trigger();
	
}