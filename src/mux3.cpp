#include <systemc.h>
#include "mux3.hpp"

void Mux3::mux_method(){
	
	switch (sel_in.read().to_uint()){
		
		case 0 :{
			out.write(data_1.read());
			break;
		}
		
		case 1 :{
			out.write(data_2.read());
			break;
		}
		
		case 2 :{
			out.write(data_3.read());
			break;
		}
		
		default:{
			out.write(data_1.read());
			break;
		}
	}
	
	next_trigger();
}