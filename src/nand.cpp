#include <systemc.h>
#include "nand.hpp"

void Nand::nand_thread(){
	
	while (true){
		wait();
		res -> write( ~(op1->read()  & op2->read())) ;
	}
}