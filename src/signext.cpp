#include <systemc.h>
#include "signext.hpp"

using namespace std;

void Sign_extend ::extend_method(){
	cout << "il bit piu significativo vale" << in7.read()[6] << endl;
	if( in7.read()[6] == 0 ){
		signext.write(concat("000000000", in7.read()));
	}
	else {
		signext.write(concat( "111111111", in7.read()));
	}
}