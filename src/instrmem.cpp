#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "instrmem.hpp"

using namespace std;

void Instruction_memory::Instruction_method(){
	if (reset.read() == 1){
		instruction.write(0) ;
	}
	else if ( clk.read() == 1){
		read_instr();
	}
}

void Instruction_memory::init(){
	
	
	
	for( int i = 0; i<SIZE_INSTR_MEM ; i++){
		switch (i){
			
			case 0: { // prima istruzione lui R2 25 ovvero " 011 010 0000011001"  R2 contiene 25
				InstrMem[i]     = "01101000";
				InstrMem[i+1] = "00011001";
				break;
			}
			case 2: {//seconda istruzione addi R1,R2,50 ovvero "001 001 010 0110010" R1 = R2+50 = 75
				InstrMem[i] = "00100101";
				InstrMem[i+1] = "00110010";
				break;
			}
			case 4:{//terza istruzione nand R3, R2, R1 ovvero "010 011 010 0000 001 " R3 = R2 and R1 
						 //lo uso per generare a caso un indirizzo di memoria
				InstrMem[i] = "01001101";
				InstrMem[i+1] ="00000001";
				break;
			}
			case 6:{//quarta istruzione add R4, R2, R1 ovvero "000 100 010 0000 001" R4 = R1+R2 = 100
				InstrMem[i] = "00010001";
				InstrMem[i+1] ="00000001";
				break;
			}
			case 8: {/* quinta istruzione sw R1, R3, 0 ovvero "101 001 011 0000000" salvo in memoria il contenuto 
							di R1 (75) nella locazione di memoria contenuta in R3 
							*/
				InstrMem[i] = "10100101";
				InstrMem[i+1] ="10000000";
				break;
			}
			case 10: {/* sesta istruzione sw R2, R3, 1 ovvero "101 010 011 0000001" salvo in memoria il contenuto
							 di R2 (25) nella locazione di memoria R3+1
						 */
				InstrMem[i] = "10101001";
				InstrMem[i+1] = "10000001";
				break;
			}
			case 12:{/*settima istruzione beq R5, R4, 4 ovvero "110 101 100 0000011" se il contenuto di R5 è uguale
							al contenuto di R4 passo all'ultima istruzine questo avviene (ovvero PC=11) avviene dopo il jalr
			*/
				InstrMem[i] ="11010110";
				InstrMem[i+1] ="00000100";
				break;
			}
			case 14:{/*ottava istruzione lw R5 , R3, 1 ovvero "100 101 011 0000001 " carico della meoria nel registro R5
							il valore contenuto all'indirizzo R3+1 quindi R5 conterra il valore 25
			*/
				InstrMem[i] = "10010101";
				InstrMem[i+1] ="10000001";
				break;
			}
			case 16:{/* nona istruzione add R5, R1, R5 ovvero "000 101 001 0000 101" R5 = R1+R5 = 75+25
							questo fa si che al secondo ciclo  la beq venga eseguita
						*/
				InstrMem[i] ="00010100";
				InstrMem[i+1] ="10000101";
				break;
			}
			case 18:{/*decima istruzione lui R7 6 ovvero " 011 111 0000000110"  R7 contiene che è l'inidirizzo
							a cui salteremo la prossiama istruzione questo perche la beq adesso avrà successo
						*/
				InstrMem[i] ="01111100";
				InstrMem[i+1] ="00000110";
				break;
			}
			case 20: {/*undicesima istruzione jalr R7, R6 ovvero "111 111 110 0000000" setta il PC al valore contenuto nel 
							registro zero (cioè 0) e salva il valore PC+1 (in questo caso 11) nel registro R6						
						*/
				InstrMem[i] ="11111011";
				InstrMem[i+1] ="10000000";
				break;
			}
			
			case 22: {/* dodicesima istruzione addi R6, R0, 33 ovvero "001 110 000 0100001" cambia il valore di 
								R6 in 33, Alla fine dell'esecuzione dovrei avere R0 = 0 R1 = 75 R2=25 R3=65526 R4 = 100 R5 = 100  
								R6 = 33 R7= 6							
							*/
				InstrMem[i] ="00111000";
				InstrMem[i+1] = "00100001";
				break;
			}
			default :{// tutte le altre celle di memoria vengono messe a 0 cioè non fa niente
				InstrMem[i] = 0;
				InstrMem[i+1] = 0;
				break;
			}
			
			
		}
		//per fare i+2 ad ogni ciclo
		i++;
	}
}

void Instruction_memory::read_instr(){
	int tmp = pc.read().to_int()*2;
	instruction.write(concat(InstrMem[tmp],InstrMem[tmp+1]));
}

sc_lv<N_BIT> Instruction_memory::getInstr( int pcval){
	return concat(InstrMem[pcval*2], InstrMem[(pcval*2)+1]);
}