#include <systemc.h>
#include "controlunit.hpp"


void ControlUnit::changeState_method(){
	if ( reset == 1){
		state = start;
		changeState_event.notify(SC_ZERO_TIME);
	}
	else if ( clk == 1 && reset == 0){
		state = nextState;
		changeState_event.notify(SC_ZERO_TIME);
	}
}

void ControlUnit::control_unit_method(){

		switch (state){
			
			case start:{
				nextState = fetch;
				loadPc.write(SC_LOGIC_0);
				loadIr.write(SC_LOGIC_0);
				loadSrc1.write(SC_LOGIC_0);
				loadSrc2.write(SC_LOGIC_0);
				loadAluout.write(SC_LOGIC_0);
				loadImm.write(SC_LOGIC_0);
				loadImm10.write(SC_LOGIC_0);
				loadMdr.write(SC_LOGIC_0);
				loadNewPc.write(SC_LOGIC_0);
				loadJump.write(SC_LOGIC_0);
				selMuxOp2Alu.write(SC_LOGIC_0);
				selMuxSrc2RegFile.write(SC_LOGIC_0);
				selMuxPc.write(0);
				selMuxWriteRegFile.write(0);
				selAluOp.write(0);
				weRegFile.write(SC_LOGIC_0);
				weDataMem.write(SC_LOGIC_0);
				readDataMem.write(SC_LOGIC_0);
				break;
			}
			
			case fetch:{
				nextState = decode;
				weDataMem.write(SC_LOGIC_0);
				loadPc.write(SC_LOGIC_0);
				weRegFile.write(SC_LOGIC_0);
				loadNewPc.write(SC_LOGIC_1);
				loadIr.write(SC_LOGIC_1);
				break;
			}
			case decode:{
				loadNewPc.write(SC_LOGIC_0);
				loadIr.write(SC_LOGIC_0);
				switch (instToControlUnit.read().to_uint()){
					case 0:{
						selMuxSrc2RegFile.write(SC_LOGIC_0);
						selMuxOp2Alu.write(SC_LOGIC_0);
						selMuxWriteRegFile.write(1);
						selMuxPc.write(0);
						selAluOp.write(0);
						nextState = aluexecution;
						break;
					}
					case 1:{
						selMuxSrc2RegFile.write(SC_LOGIC_0);
						selMuxOp2Alu.write(SC_LOGIC_1);
						selMuxWriteRegFile.write(1);
						selMuxPc.write(0);
						selAluOp.write(0);
						nextState = aluexecution;
						break;
					}
					case 2: {
						selMuxSrc2RegFile.write(SC_LOGIC_0);
						selMuxOp2Alu.write(SC_LOGIC_0);
						selMuxWriteRegFile.write(1);
						selMuxPc.write(0);
						selAluOp.write(1);
						nextState = aluexecution;
						break;
					}
					case 3:{
						selMuxSrc2RegFile.write(SC_LOGIC_0);
						selMuxOp2Alu.write(SC_LOGIC_0);
						selMuxWriteRegFile.write(3);
						selMuxPc.write(0);
						selAluOp.write(0);
						nextState = writeback;
						break;
					}
					case 4:{
						selMuxSrc2RegFile.write(SC_LOGIC_0);
						selMuxOp2Alu.write(SC_LOGIC_1);
						selMuxWriteRegFile.write(2);
						selMuxPc.write(0);
						selAluOp.write(0);
						nextState = aluexecution;
						break;
					}
					case 5:{
						selMuxSrc2RegFile.write(SC_LOGIC_1);
						selMuxOp2Alu.write(SC_LOGIC_1);
						selMuxWriteRegFile.write(0);
						selMuxPc.write(0);
						selAluOp.write(0);
						nextState = aluexecution;
						break;
					}
					case 6:{
						selMuxSrc2RegFile.write(SC_LOGIC_1);
						selMuxOp2Alu.write(SC_LOGIC_0);
						selMuxWriteRegFile.write(0);
						selMuxPc.write(0);
						selAluOp.write(3);
						nextState = beqstate;
						break;
					}
					case 7:{
						selMuxSrc2RegFile.write(SC_LOGIC_1);
						selMuxOp2Alu.write(SC_LOGIC_0);
						selMuxWriteRegFile.write(0);
						selMuxPc.write(2);
						selAluOp.write(2);
						nextState = aluexecution;
						break;
					}
				 
				 
				 }
				 loadImm.write(SC_LOGIC_1);
				loadImm10.write(SC_LOGIC_1);
				loadJump.write(SC_LOGIC_1);
				loadSrc1.write(SC_LOGIC_1);
				loadSrc2.write(SC_LOGIC_1);
				 break;
			}
			case aluexecution:{
				loadImm.write(SC_LOGIC_0);
				loadImm10.write(SC_LOGIC_0);
				loadJump.write(SC_LOGIC_0);
				loadSrc1.write(SC_LOGIC_0);
				loadSrc2.write(SC_LOGIC_0);
				
				loadAluout.write(SC_LOGIC_1);
				switch(instToControlUnit.read().to_uint()){
					case 0:
					case 1:
					case 2:{
						nextState = writeback;
						break;
					}
					case 4: {
						nextState = memaccessread;
						break;
					}
					case 5:{
						nextState = memaccesswrite;
						break;
					}
					case 7: {
						nextState = jump;
						break;
					}
				}
				break;
			}
			
			case jump:{
				loadAluout.write(SC_LOGIC_0);
				weRegFile.write(SC_LOGIC_1);
				loadPc.write(SC_LOGIC_1);
				nextState = fetch;
				break;
			}
			case beqstate : {
				loadImm.write(SC_LOGIC_0);
				loadImm10.write(SC_LOGIC_0);
				loadJump.write(SC_LOGIC_0);
				loadSrc1.write(SC_LOGIC_0);
				loadSrc2.write(SC_LOGIC_0);
				if (isZero.read().to_bool() == 1){//sono uguali
					selMuxPc.write(1);
					loadPc.write(SC_LOGIC_1);
				}
				else { //diversi
					selMuxPc.write(0);
					loadPc.write(SC_LOGIC_1);
				}
				nextState = fetch;
				break;
			}
			case memaccessread:{
				loadAluout.write(SC_LOGIC_0);
				
				nextState = writeback;
				
				readDataMem.write(SC_LOGIC_1);
				loadMdr.write(SC_LOGIC_1);
				break;
			}
			
			case memaccesswrite:{
				loadAluout.write(SC_LOGIC_0);

				nextState = fetch;
				
				weDataMem.write(SC_LOGIC_1);
				loadPc.write(SC_LOGIC_1);
				
				break;
			}
			
			case writeback:{
				loadJump.write(SC_LOGIC_0);
				loadAluout.write(SC_LOGIC_0);
				readDataMem.write(SC_LOGIC_0);
				loadMdr.write(SC_LOGIC_0);
				
				nextState = fetch;
				
				weRegFile.write(SC_LOGIC_1);
				loadPc.write(SC_LOGIC_1);
				
				break;
			}
			
		}
}

void ControlUnit::init(){
	
	state= start;
	nextState = fetch;
	
}

void ControlUnit::print_state(){
	cout << "al tempo: \t" << sc_time_stamp() << endl;
	switch (state){
		case start:cout << "siamo nello stato: \t start" << endl; 										break;
		case fetch:cout << "siamo nello stato: \t fetch" << endl;                                   break;
		case decode:cout << "siamo nello stato: \t decode" << endl;                            break;
		case aluexecution:cout << "siamo nello stato: \t aluexecution" << endl;              break;
		case jump:cout << "siamo nello stato: \t jump" << endl;                                    break;
		case beqstate:cout << "siamo nello stato: \t beqstate" << endl;                         break;
		case memaccessread:cout << "siamo nello stato: \t memaccessread" << endl;  break;
		case memaccesswrite:cout << "siamo nello stato: \t memaccesswrite" << endl; break;
		case writeback:cout << "siamo nello stato: \t writeback" << endl;                      break;
	}
	switch (nextState){
		case start:cout << "il prossimo stato: \t start" << endl; 										break;
		case fetch:cout << "il prossimo stato: \t fetch" << endl;                                   break;
		case decode:cout << "il prossimo stato: \t decode" << endl;                            break;
		case aluexecution:cout << "il prossimo stato: \t aluexecution" << endl;              break;
		case jump:cout << "il prossimo stato: \t jump" << endl;                                    break;
		case beqstate:cout << "il prossimo stato: \t beqstate" << endl;                         break;
		case memaccessread:cout << "il prossimo stato: \t memaccessread" << endl;  break;
		case memaccesswrite:cout << "il prossimo stato: \t memaccesswrite" << endl; break;
		case writeback:cout << "il prossimo stato: \t writeback" << endl;                      break;
	}
	
}