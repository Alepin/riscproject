#include <systemc.h>
#include "signext10.hpp"

using namespace std;

void Sign_extend10 ::extend_method10(){
	cout << "il bit piu significativo vale" << in10.read()[9] << endl;
	if( in10.read()[9] == 0 ){
		signext10.write(concat("000000000", in10.read()));
	}
	else {
		signext10.write(concat( "111111111", in10.read()));
	}
}