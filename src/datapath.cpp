#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "datapath.hpp"

void Datapath::datapath_method(){
	 
	 instToControlUnit.write( instruction.read().range(15,13));
	 src2first.write(irOut.read().range(2,0));
	 src2second.write(irOut.read().range(12,10));
	 src1fromIr.write(irOut.read().range(9,7));
	 tgtfromIr.write(irOut.read().range(12,10));
	 ext7Out	.write(irOut.read().range(6,0));
	 ext10Out.write(irOut.read().range(9,0));
	 
	 /*cout << "uscita ir " << instrMem.instruction.read() << "\t" << sc_time_stamp() <<endl;
	 cout << "segnale di load " << loadIr.read() << "\t" << sc_time_stamp() <<endl;
	 printRegValue();
	 //printRegisterFile();*/
	next_trigger();
}

void Datapath::printRegisterFile(){
	for ( int i = 0 ; i<8; i++){
		cout << "Registro all'indirizzo: " << i << " contiene il valore : "<< regFile.getRegisterValue(i) << endl;
	}
}
void Datapath::getRegisterFile( int vector[] ){
	for ( int i = 0 ; i<8; i++){
		vector[i] = regFile.getRegisterValue(i);
	}
}
void Datapath::printMemValue ( int address){
	
	cout << "All'indirizzo: " << address << " c'è il valore: " << Ram.getMemValue( address) <<  endl;
	
}

int Datapath::getMemValue (int address){
	
	return Ram.getMemValue( address);
		
}


void Datapath::getRegValue(int vector[]){
	
	vector[0] = PC			.getRegValue()		;
	vector[1] = newPc		.getRegValue()		;
	vector[2] = jumpReg	.getRegValue()		;
	vector[3] = iReg		.getRegValue()		;
	vector[4] = imm			.getRegValue()		;
	vector[5] = imm10		.getRegValue()		;
	vector[6] = src1			.getRegValue()		;
	vector[7] = src2			.getRegValue()		;
	vector[8] = aluOutReg.getRegValue()		;
	vector[9] = mdr			.getRegValue()		;
	
}

void Datapath::printRegValue(){
	
	cout << "valore di PC \t" << PC						.getRegValue()<< endl;
	cout << "valore di newPc \t" << newPc			.getRegValue()<< endl;
	cout << "valore di jump reg \t" << jumpReg		.getRegValue()<< endl;
	cout << "valore di istr reg \t" << iReg				.getRegValue()<< endl;
	cout << "valore di imm reg: \t " << imm			.getRegValue()<< endl;
	cout << "valore di imm10 reg\t" << imm10		.getRegValue()<< endl;
	cout << "valore di src1 reg \t" << src1				.getRegValue()<< endl;
	cout << "valore di src2 reg \t" << src2				.getRegValue()<< endl;
	cout << "valore di aluout reg \t" << aluOutReg	.getRegValue()<< endl;
	cout << "valore di MDR reg \t" << mdr				.getRegValue()<< endl;
	cout << "istruzione" << instToControlUnit.read() << endl;
}