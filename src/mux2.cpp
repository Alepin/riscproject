#include <systemc.h>
#include "mux2.hpp"

void Mux2::mux_method(){
	
	switch (sel_in.read().to_bool()){
		
		case 0 :{
			out.write(data_1.read());
			break;
		}
		
		case 1 :{
			out.write(data_2.read());
			break;
		}
		
		default:{
			out.write(data_1.read());
			break;
		}
	}
	
	next_trigger();
}