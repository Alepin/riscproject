#include <systemc.h>
#include "datamem.hpp"

void Data_memory::datamem_method(){
	if(reset.read()==1){
		for (int i=0; i<SIZE_DATA_MEM; i++){
			DataMem[i] =0;
		}
	}
	else if (clk.read() == 1 && wedatamem.read() == 1){
		 writeDataMem();
	}
	else if (clk.read() == 1 && readmem.read() == 1){
	  
	  readDataMem();
	  
	}
	  
	  next_trigger();
	
}

int Data_memory::getMemValue( unsigned add){
	
	return DataMem[add].to_int();
	
}

void Data_memory::writeDataMem(){
	
	DataMem[address.read().to_uint()] = datain.read();
	
}

void Data_memory::readDataMem(){
	
	dataout.write(DataMem[address.read().to_uint()]);
	
}