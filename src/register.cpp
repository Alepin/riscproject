#include <systemc.h>
#include "register.hpp"

void Reg::reg_method(){
	
	if (reset.read() == 1){
		value = 0;
	}
	else if (load.read() == 1 && clk.read() == 1){
		value = datain.read();
	}
	dataout.write(value);
	next_trigger();
}

int Reg::getRegValue(){
	
	return value.to_uint();
	
}