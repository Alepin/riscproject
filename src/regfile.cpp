#include <systemc.h>
#include <iostream>
#include "regfile.hpp"

using namespace std;

void Register_file::regfile_method(){
	
	if (reset.read() == 1){
		
		for (int i = 0; i<SIZE_MEM ; i++){
			RegMem[i] = 0;
		}
		//readRegFile();
	}
	else if ( clk.read() == 1 && we.read() == 1){
		//operazione di scrittura
		writeRegFile();
	}
	
	readRegFile();
	
	next_trigger();
	
}

int Register_file::getRegisterValue(int address){
	
	return RegMem[address].to_uint();
	
}

void Register_file::writeRegFile(){
	
	if ( address_write.read().to_uint() == 0){
		cout  << "ERRORE non si può effetuare una scrittura nel registro 0!" << endl;
	}
	else{
		RegMem[address_write.read().to_uint()] = data_write.read();
	}
	
}

void Register_file::readRegFile(){
	
	dataout1.write(RegMem[address1.read().to_uint()]);
	dataout2.write(RegMem[address2.read().to_uint()]);
	
}