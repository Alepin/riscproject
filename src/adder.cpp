#include <systemc.h>
#include "adder.hpp"

using namespace sc_core;

void Adder::add(){
	while(true){

		wait();
		res->write( op1->read().to_int() + op2->read().to_int());

	}
}
