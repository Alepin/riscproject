#include <systemc.h>
#include "mux2_3bit.hpp"

void Mux2_3bit::mux_method(){
	
	switch (sel_in.read().to_bool()){
		
		case 0 :{
			out.write(data_1.read());
			break;
		}
		
		case 1 :{
			out.write(data_2.read());
			break;
		}
		
		default:{
			out.write(data_1.read());
			break;
		}
	}
	
	next_trigger();
}