#include <systemc.h>
#include "eqzero.hpp"

void Eq_zero::zero_method(){
	
	sc_lv<N_BIT> tmp = ~(ingresso.read());
	is_zero->write(tmp.and_reduce());
	next_trigger();
	
}