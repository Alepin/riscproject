#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "nand.hpp"

using namespace std;

SC_MODULE (TestBench) {
	
	static const unsigned SIZE = 10;
	static const unsigned N_MAX = 16384; 
	sc_int<N_BIT> ing1[SIZE] , ing2[SIZE], result[SIZE];
	sc_signal < sc_lv<N_BIT> > sigin1, sigin2;
	sc_signal < sc_lv<N_BIT> > sigout;
	
	Nand nand_test;
	
	SC_CTOR(TestBench) : nand_test("nand_test")
	{
		init_values();
		SC_THREAD(stimulus_thread);
		nand_test.op1(sigin1);
		nand_test.op2(sigin2);
		nand_test.res(sigout);
		
	}
	
	bool check(){
		bool error = 0;
		for (int i =0 ; i<SIZE ; i++){
			if (~(ing1[i]&ing2[i]) != result[i])
				error = 1;
		}
		return error;
	}
	
	private:
	
	void stimulus_thread(){
		
		for(int i=0 ; i<SIZE ; i++){
			sigin1.write(ing1[i]);
			sigin2.write(ing2[i]);
			wait(10, SC_US);
			result[i] = sigout.read().to_int();
			cout << sigin1.read() << " and " << endl  << sigin2.read() << endl << "risultato: " << sigout.read().to_int() << endl;
		}
	}
	
	void init_values(){
		srand(time(NULL));
		for(int i=0; i<SIZE; i++){
			ing1[i] = rand()%N_MAX;
			ing2[i] = rand()%N_MAX;
			cout << ing1[i] << " and " << ing2[i] << " = " <<  (~(ing1[i]&ing2[i] )) << endl;
		}
	}
	
};

int sc_main( int argv, char** argc){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}