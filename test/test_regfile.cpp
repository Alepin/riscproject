#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "regfile.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const unsigned N_MAX = 100;
	
	sc_signal<sc_lv<3> > add1 ;
	sc_signal<sc_lv<3> > add2;
	sc_signal<sc_lv<3> > add_wr;
	sc_signal<sc_lv<N_BIT> > data_wr;
	sc_signal<sc_lv<N_BIT> > data1;
	sc_signal<sc_lv<N_BIT> > data2;
	sc_signal<sc_logic>  we;
	sc_signal<bool > reset;
	sc_in<bool> clk ;
	
	sc_event stim_event;
	
	int datawrite[16];
	bool error;
	
	Register_file reg_test;
	
	SC_CTOR(TestBench) : reg_test("reg_test"){
		init_values();
		SC_THREAD(stimulus_thread);
			sensitive << clk.pos();
			dont_initialize();
		
		SC_METHOD(observer_method)
			sensitive << stim_event;
			dont_initialize();
			
		//collegamento porte
		reg_test.address1(this -> add1);
		reg_test.address2(this -> add2);
		reg_test.address_write(this -> add_wr);
		reg_test.data_write(this -> data_wr);
		reg_test.dataout1(this -> data1);
		reg_test.dataout2(this -> data2);
		reg_test.we(this -> we);
		reg_test.reset(this -> reset);
		reg_test.clk(this -> clk);
	}
	
	
	bool check(){
		
		return error;
		
	}
	
	void printReg() {
		for (int i =0; i<SIZE_MEM ; i++)
			cout << "Registro all'indirizzo: " << i << " contiene il valore : "<< reg_test.getRegisterValue(i) << endl;
	}
	
	private: 
	int chekker;
	
	void observer_method(){
		
		
		cout << "observer_method run!" << endl;
		printReg();
		cout << "primo indirizzo " << add1.read().to_uint() << "  secondo indirizzo " << add2.read().to_uint()<< endl;
		cout << "uscita 1 = " << data1.read().to_int() << " uscita 2 = " << data2.read().to_int() << endl <<endl;
		
		if ( reg_test.getRegisterValue(add1.read().to_uint()) != data1.read().to_int())
			error = 1;
		if ( reg_test.getRegisterValue(add2.read().to_uint()) != data2.read().to_int())
			error = 1;
		if (reg_test.getRegisterValue(0) != 0 )
			error = 1;
		
		switch (chekker){
			 case 1: case 2: case 3: case 4: case 5: case 6: case 7: {
				for (int n = 1; n<(chekker+1) ; n++){
					if (reg_test.getRegisterValue(n) != datawrite[n-1] ){
						error = 1;
					}
				}
				for (int n = chekker+1 ; n < SIZE_MEM; n++){
					if (reg_test.getRegisterValue(n) != 0){
						error =1;
					}
				}
				break;
			}
			
			case 8:{ //reset
				for (int n=1 ; n<SIZE_MEM ; n++){
					if (reg_test.getRegisterValue(n) != 0){
						error =1;
					}
				}
				break;
			}
			
			case 9: case 10: case 11: case 12: case 13: case 14: case 15: {
				for ( int n = 1 ; n < (chekker+1-SIZE_MEM); n++){
					if (reg_test.getRegisterValue(n) != datawrite[n+SIZE_MEM-1]){
						error = 1;
					}
				}
				for (int n = (chekker-SIZE_MEM+1); n<SIZE_MEM; n++){
					if(reg_test.getRegisterValue(n) != 0){
						error = 1;
					}
				}
				break;
			}
			
			case 16:{// cerca di scrivere nel registro 0
				for (int n = 1 ; n< SIZE_MEM; n++){
					if (reg_test.getRegisterValue(n) != datawrite[n+SIZE_MEM-1])
						error = 1;
				}
				break;
			}
			
			default : {
				error = 1;
				break;
			}
		}
		
		next_trigger();
		
	}
	
	void stimulus_thread(){
		cout << "stim start" << endl;
		reset.write(0);
		wait(SC_ZERO_TIME);
		printReg();
		wait();
		reset.write(0);
		wait(SC_ZERO_TIME);
		cout << endl << "enter cycle" << endl;
		for(int i = 0 ; i< 16 ; i++){
				chekker ++;
				// faccio delle write sul registro invio l'evento di write
				we.write(SC_LOGIC_1);
				data_wr.write(datawrite[i]);
				add1.write(rand()%SIZE_MEM);
				add2.write(rand()%SIZE_MEM);
				
				
				if ( i < 7 ){
					
					add_wr.write((i+1));
					
				}
				else if (i == 7) {
					
					cout << " attivazione reset! " << endl;
					add_wr.write(rand()%SIZE_MEM);
					reset.write(1);
					
				}
				else {
					
					add_wr.write((i+1)%SIZE_MEM);

				}
				//aspetto 10 ns per avere i segnali al valore desiderato (si potrebbe usare anche un SC_ZERO_TIME)
				wait (10, SC_NS);
	
				cout << "stimulus event " << sc_time_stamp() << endl;
				cout << "wait clk pos" << endl;
				stim_event.notify(SC_ZERO_TIME);
				
				we.write(SC_LOGIC_0);
				
				wait ();
				
				cout << "clock rising edge " << sc_time_stamp() << endl;
				reset.write(0);
		}
	}
	
	void init_values(){
		// azzero i segnali per evitare warning
		add1.write(0);
		add2.write(0);
		add_wr.write(0);
		data_wr.write(0);
		error = 0;
		chekker = 0;
		srand(time(NULL));
		for (int i =0; i<16 ; i++){
			datawrite[i] = rand()%N_MAX;
			cout << datawrite[i] << endl;
		}
	}
	
	
};

int sc_main ( int argc, char** argv){
	
	sc_clock clock ("clk", 100, SC_NS);
	
	TestBench test("test");
	test.clk(clock);
	
	cout << "INIZIA TUTTO!!!!!!" << endl;
	sc_start( 2000 , SC_NS);
	
	cout << test.check() << endl;
	
	return test.check();
	
	
}