#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "signext.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static unsigned const MAX = 128;
	
	sc_signal<sc_lv<BIT_IN> > in7;
	sc_signal<sc_lv< N_BIT> > signext;
	
	bool error;
	int stimVec[10];
	int chekker;
	sc_event stim_event;
	
	Sign_extend signext_test;
	
	SC_CTOR(TestBench) : signext_test("signext_test"){
		init();
		
		SC_THREAD(stim_thread);
		
		SC_METHOD(obs_method);
			sensitive << stim_event;
			dont_initialize();
		
		//collegamento porte
		signext_test.in7(this -> in7);
		signext_test.signext (this -> signext);
		
	}
	
	bool check(){
		return error;
	}
	
	private:
	void obs_method(){
		
		cout << "ingresso vale " << in7.read() << "\t uscita vale: " << signext.read() << "\t al tempo: " << sc_time_stamp() << endl;
		
		if ( in7.read().to_int() == signext.read().to_int())
			error = error or 0;
		else 
			error = error or 1;
		
	}
	
	void stim_thread(){
		for ( int i = 0; i<10 ; i++){
			
			in7.write(stimVec[i]);
			wait(SC_ZERO_TIME);
			stim_event.notify(SC_ZERO_TIME);
			wait (100, SC_NS);
			
		}
	}
	
	void init(){
		chekker = 0;
		error = 0;
		srand(time(NULL));
		for (int i=0 ; i<10 ; i++){
			stimVec[i] = rand()%MAX;
		}
	}
	
};

int sc_main ( int argc, char** argv){
	
	TestBench test("test");
	
	cout << "INIZIO TEST!!!" << endl;
	
	sc_start();
	
	return test.check();
	
}