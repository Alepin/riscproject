#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "substract.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const int SIZE = 10;
	static const int N_MAX = 16384;
	sc_signal <sc_lv<N_BIT> > in1, in2;
	sc_signal<sc_lv<N_BIT> > sub_out;
	int ing1[SIZE], ing2[SIZE], result[SIZE];
	
	Substract sub_test;
	
	SC_CTOR(TestBench) : sub_test("sub_test"){
		//inizializzazione e metodi
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		sub_test.op1(in1);
		sub_test.op2(in2);
		sub_test.res_sub(sub_out);
	}
	
	bool check(){
		bool error = 0;
		for(int i =0 ; i<SIZE ; i++){
			if (ing1[i] - ing2[i] != result[i])
				error = 1;
		}
		return error;
	}
	
	private:
	
	void stimulus_thread(){
		for ( int i = 0; i<SIZE ; i++){
			in1.write(ing1[i]);
			in2.write(ing2[i]);
			wait(10, SC_US);
			result[i] = sub_out.read().to_int();
			cout << in1.read().to_int() << " - " << in2.read().to_int() << " = " << sub_out.read().to_int() <<  endl;
		}
		
	}
	
	void init_values(){
		srand(time(NULL));
		for (int i=0 ; i<SIZE ; i++){
			ing1[i] = rand()%N_MAX;
			ing2[i] = rand()%N_MAX;
		}
	}
	
};

int sc_main (int argc , char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}