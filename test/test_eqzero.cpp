#include <systemc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "eqzero.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const int SIZE = 10;
	static const int N_MAX = 16384;
	sc_signal <sc_lv<N_BIT> > in;
	sc_signal<sc_logic> zero;
	int ing[SIZE];
	bool result[SIZE];
	
	Eq_zero zero_test;
	
	SC_CTOR(TestBench) : zero_test("zero_test"){
		//inizializzazione e thread
		init_value();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		zero_test.ingresso(in);
		zero_test.is_zero(zero);
	}
	
	bool check(){
		bool error = 0;
		for (int i =0; i<SIZE ; i++){
			cout << "ingresso è: " << ing[i] << " e l'usita risulta: " << result[i] << endl;
			if (~(result [i]) == (ing[i] or 0))
				error = 1;
		}
		cout <<  "valore di error: " << error << endl;
		return error;
	}
	
	private: 
	
	void stimulus_thread(){
		
		for(int i = 0; i<SIZE ; i++){
			in.write(ing[i]);
			wait(10,SC_US);
			result[i] = zero.read().to_bool();
		}
		
	}
	
	void init_value(){
		srand(time(NULL));
		
		for(int i=0; i<SIZE ; i++){
			if (i%2==0)
				ing[i] = 0;
			else 
				ing[i] = rand();
		}
	}
	
	
};

int sc_main(int argc, char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
}