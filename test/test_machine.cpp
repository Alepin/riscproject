#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "datapath.hpp"
#include "controlunit.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	sc_signal<bool> reset;
	sc_signal<sc_logic> loadPc2;
	sc_signal<sc_logic> loadIr;
	sc_signal<sc_logic> loadSrc1;
	sc_signal<sc_logic> loadSrc2;
	sc_signal<sc_logic> loadAluout;
	sc_signal<sc_logic> loadImm;
	sc_signal<sc_logic> loadImm10;
	sc_signal<sc_logic> loadMdr;
	sc_signal<sc_logic> loadNewPc;
	sc_signal<sc_logic> loadJump;
	sc_signal<sc_logic> selMuxOp2Alu;
	sc_signal<sc_logic> selMuxSrc2RegFile;
	sc_signal<sc_lv<2> > selMuxPc;
	sc_signal<sc_lv<2> > selMuxWriteRegFile;
	sc_signal<sc_lv<2> > selAluOp;
	sc_signal<sc_logic> weRegFile;
	sc_signal<sc_logic> weDataMem;
	sc_signal<sc_logic> readDataMem;
	sc_signal<sc_logic> isZero;
	sc_signal<sc_lv<3> > instToControlUnit;
	sc_in<bool> clk;
	
	
	ControlUnit 	ctrlunit;
	Datapath		datapath;
	
	int registerFileStatus[8], otherRegisterStatus[10];
	int memoryValue1, memoryValue2;
	int address;
	bool error;
	
	sc_event stim_event;
	
	SC_CTOR(TestBench) : datapath("datapath"), ctrlunit("ctrlunit"){
		
		init();
		
		
		SC_THREAD(stim_thread);
			sensitive << clk.pos();
			dont_initialize();
		
		SC_METHOD(obs_method);
			sensitive << stim_event;
			dont_initialize();
		
		// collegamento controlunit e datapath
		ctrlunit.clk(this ->clk);
		ctrlunit.reset(this ->reset);
		ctrlunit.loadPc(this -> loadPc2);
		ctrlunit.loadIr	(this ->loadIr);
		ctrlunit.loadSrc1					(this ->loadSrc1);
		ctrlunit.loadSrc2					(this ->loadSrc2);
		ctrlunit.loadAluout				(this ->loadAluout				 );
		ctrlunit.loadImm					(this ->loadImm					 );
		ctrlunit.loadImm10				(this ->loadImm10				 );
		ctrlunit.loadMdr					(this ->loadMdr					 );
		ctrlunit.loadNewPc				(this ->loadNewPc				 );
		ctrlunit.loadJump					(this ->loadJump					 );
		ctrlunit.selMuxOp2Alu			(this ->selMuxOp2Alu			 );
		ctrlunit.selMuxSrc2RegFile	(this ->selMuxSrc2RegFile	 );
		ctrlunit.selMuxPc					(this ->selMuxPc					 );
		ctrlunit.selMuxWriteRegFile	(this ->selMuxWriteRegFile	 );
		ctrlunit.selAluOp					(this ->selAluOp					 );
		ctrlunit.weRegFile				(this ->weRegFile				 );
		ctrlunit.weDataMem				(this ->weDataMem				 );
		ctrlunit.readDataMem			(this ->readDataMem			 );
		ctrlunit.isZero						(this ->isZero						 );
		ctrlunit.instToControlUnit		(this ->instToControlUnit		);
		
		
		datapath.clk(this ->clk);
		datapath.reset(this ->reset);
		datapath.loadPc (this ->loadPc2);
		datapath.loadIr (this ->loadIr);
		datapath.loadSrc1(this ->loadSrc1);
		datapath.loadSrc2(this ->loadSrc2);
		datapath.loadAluout(this ->loadAluout);
		datapath.loadImm(this ->loadImm	);
		datapath.loadImm10(this ->loadImm10);
		datapath.loadMdr(this ->loadMdr);
		datapath.loadNewPc(this ->loadNewPc);
		datapath.loadJump	(this ->loadJump);
		datapath.selMuxOp2Alu	(this ->selMuxOp2Alu );
		datapath.selMuxSrc2RegFile(this ->selMuxSrc2RegFile );
		datapath.selMuxPc					(this ->selMuxPc );
		datapath.selMuxWriteRegFile	(this ->selMuxWriteRegFile	 );
		datapath.selAluOp					(this ->selAluOp					 );
		datapath.weRegFile				(this ->weRegFile				 );
		datapath.weDataMem				(this ->weDataMem				 );
		datapath.readDataMem			(this ->readDataMem			 );
		datapath.isZero						(this ->isZero						 );
		datapath.instToControlUnit		(this ->instToControlUnit		 );
		
		
		
	}
	
	bool check(){
		datapath.getRegisterFile( registerFileStatus);
	
		memoryValue1 = datapath.getMemValue( 65526);
		memoryValue2 = datapath.getMemValue( 65527);

		if ( memoryValue1 != 75 || memoryValue2 != 25)
			 error = 1;
		if ( registerFileStatus[0] != 0 || registerFileStatus[1] != 75 || registerFileStatus[2] != 25 ||registerFileStatus[3] != 65526 ||registerFileStatus[4] != 100 )
			error = 1;
		if ( registerFileStatus[5] != 100 || registerFileStatus[6] != 33 || registerFileStatus[7] != 6 )		
			error =1;
		
		return error;
	}
	
	private:
	
	void init(){
		reset.write(0);
		error = 0;
	}
	
	void stim_thread(){
		reset.write(1);
		stim_event.notify(1 ,SC_NS);
		wait (50, SC_NS);
		stim_event.notify(1 ,SC_NS);
		wait();
		reset.write(0);
		for (;;){
			
			stim_event.notify(1 ,SC_NS);
			wait();
		}
	}
	
	void obs_method(){
		
		
		ctrlunit.print_state();
		datapath.printRegisterFile();
		datapath.printRegValue();
		
	    cout << "la cella di memoria all'indirizzo 65526 contiene : " << datapath.Ram.getMemValue( 65526) << endl;
		cout << "la cella di memoria all'indirizzo 65526 contiene : " << datapath.Ram.getMemValue( 65527) << endl << endl;
	}
	
};


int sc_main( int argc , char** argv){
	sc_clock clk ("clk" , 100 , SC_NS);
		
	TestBench test("test");
	test.clk(clk);
	cout << "INIZIO TEST!!!" << endl;
	
	sc_start(5000 , SC_NS);
	
	cout << test.check() << endl;
	
	return test.check();
}