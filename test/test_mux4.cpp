#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "mux4.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const unsigned SIZE = 10;
	static const unsigned N_MAX = 16384;
	static const unsigned N_SELECT = 4;
	
	int data_in1[SIZE], data_in2[SIZE], data_in3[SIZE],data_in4[SIZE], result[SIZE], sel[SIZE];
	
	sc_signal<sc_lv<N_BIT> > din1,din2,din3,din4, output;
	sc_signal<sc_lv<2> > selector;
	
	Mux4 mux_test;
	
	SC_CTOR(TestBench) : mux_test("mux_test")
	{
		//inizializzazione e thread di stimolo
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		mux_test.data_1(din1);
		mux_test.data_2(din2);
		mux_test.data_3(din3);
		mux_test.data_4(din4);
		mux_test.sel_in(selector);
		mux_test.out(output);
	}
	
	bool check(){
		bool error = 0;
		for (int i =0; i<SIZE ; i++){
			switch (sel[i]){
				case 0:{
					if (result[i] != data_in1[i])
						error = 1;
					break;
				}
				
				case 1:{
					if (result[i] != data_in2[i] )
						error = 1;
					break;
				}
				
				case 2:{
					if (result[i] != data_in3[i])
						error = 1;
					break;
				}
				
				case 3:{
					if (result[i] != data_in4[i])
						error = 1;
					break;
				}
				
				default :{
					error = 1;
					cout << "errore c'e un valore di select maggiore di quello consentito." << endl;
				}
				
			}
		}
		if ( error == 0)
			cout << "test completato con successo!" << endl;
		else 
			cout << "errore durante la fase di test!" << endl;
		
		return error;
	}
	
	private : 
	void stimulus_thread(){
		for(int i = 0 ; i<SIZE ; i++){
			din1.write(data_in1[i]);
			din2.write(data_in2[i]);
			din3.write(data_in3[i]);
			din4.write(data_in4[i]);
			selector.write(sel[i]);
			wait (10,SC_US);
			result[i] = output.read().to_int();
			cout << "Primo ingresso: " << din1.read().to_int() << " Secondo ingresso: " << din2.read().to_int() << " Terzo ingresso: " 
			<< din3.read().to_int() << "quarto ingresso: " << din4.read().to_int() << " Selettore. " << selector.read().to_uint() << " Risultato: " << output.read().to_int() << endl;
		}
	}
	
	void init_values(){
		srand(time(NULL));
		for(int i = 0; i<SIZE; i++){
			data_in1[i] = rand()%N_MAX;
			data_in2[i] = rand()%N_MAX;
			data_in3[i] = rand()%N_MAX;
			data_in4[i] = rand()%N_MAX;
			sel[i] = rand()%N_SELECT;
		}
	}
	
};

int sc_main(int argc, char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}