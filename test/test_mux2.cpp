#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "mux2.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const unsigned SIZE = 10;
	static const unsigned N_MAX = 16384;
	static const unsigned N_SELECT = 2;
	
	int data_in1[SIZE], data_in2[SIZE], result[SIZE];
	sc_logic sel[SIZE];
	
	sc_signal<sc_lv<N_BIT> > din1,din2, output;
	sc_signal<sc_logic > selector;
	
	Mux2 mux_test;
	
	SC_CTOR(TestBench) : mux_test("mux_test")
	{
		//inizializzazione e thread di stimolo
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		mux_test.data_1(din1);
		mux_test.data_2(din2);
		mux_test.sel_in(selector);
		mux_test.out(output);
	}
	
	bool check(){
		bool error = 0;
		for (int i =0; i<SIZE ; i++){
			switch (sel[i].to_bool()){
				case 0:{
					if (result[i] != data_in1[i])
						error = 1;
					break;
				}
				
				case 1:{
					if (result[i] != data_in2[i] )
						error = 1;
					break;
				}
				
				default :{
					error = 1;
					cout << "errore c'e un valore di select maggiore di quello consentito." << endl;
				}
				
			}
		}
		if ( error == 0)
			cout << "test completato con successo!" << endl;
		else 
			cout << "errore durante la fase di test!" << endl;
		
		return error;
	}
	
	private : 
	void stimulus_thread(){
		for(int i = 0 ; i<SIZE ; i++){
			din1.write(data_in1[i]);
			din2.write(data_in2[i]);
			selector.write(sel[i]);
			wait (10,SC_US);
			result[i] = output.read().to_int();
			cout << "Primo ingresso: " << din1.read().to_int() << " Secondo ingresso: " << din2.read().to_int() << " Selettore. " << selector.read() << " Risultato: " << output.read().to_int() << endl;
		}
	}
	
	void init_values(){
		srand(time(NULL));
		for(int i = 0; i<SIZE; i++){
			data_in1[i] = rand()%N_MAX;
			data_in2[i] = rand()%N_MAX;
			int tmp = rand()%N_SELECT;
			sel[i] = tmp;
		}
	}
	
};

int sc_main(int argc, char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}