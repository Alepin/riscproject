#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "alu.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const unsigned SIZE = 8;
	static const unsigned N_MAX = 16384;
	static const unsigned N_MAX_OP = 4;
	sc_signal< sc_lv<N_BIT> >datain1, datain2;
	sc_signal<sc_lv< N_ALU_OP> > selop;
	sc_signal<sc_lv<N_BIT> > dataout;
	sc_signal<sc_logic> compout;
	
	sc_int<N_BIT> ing1[SIZE], ing2[SIZE];
	sc_uint<N_BIT> sel[SIZE];
	sc_int<N_BIT> result[SIZE];
	bool eq[SIZE];
	
	Alu alu_test;
	
	SC_CTOR(TestBench) : alu_test("alu_test"){
		//inizializzazione e thread di stimolo
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		alu_test.opalu1(datain1);
		alu_test.opalu2(datain2);
		alu_test.selopalu(selop);
		alu_test.res_alu(dataout);
		alu_test.equal(compout);
	
	}
	
	bool check(){
		bool error = 0 ;
		for(int i= 0 ; i<SIZE ; i++){
			cout << "ciclo numero: " << i << endl;
			switch(i%4){
				
				// operazione add
				case 0:{
					if (result[i] != ing1[i]+ing2[i])
						error = 1;
					cout << error << endl;
					break;
				}
				
				// operazione nand
				case 1: {
					if (result[i] != ~(ing1[i] & ing2[i]))
						error=1;
					cout << error << endl;
					cout << (~(ing1[i] & ing2[i]))  << endl;
					break;
				}
				
				//operazione pass
				
				case 2: {
					if ( result[i] != ing1[i])
						error = 1;
					cout << error << endl;
					break;
				}
				
				case 3: {
					if ( ~eq[i] == ((ing1[i]-ing2[i]) or 0))
						error = 1;
					cout << error << endl;
					break;
				}
				
				default : {
					cout << "errore selezione operazione non valida!" << endl;
					cout << error << endl;
				}
			}
			
		}
		if (error == 1)
			cout << "errore nel testing della alu!" << endl;
		else 
			cout << "Test eseguito con successo!" << endl;
		
		return error;
		
	}
	
	private : 
	void stimulus_thread(){
		
		for(int i= 0 ; i<SIZE ; i++){
			
			datain1.write(ing1[i]);
			datain2.write(ing2[i]);
			selop.write(sel[i]);
			wait (10,SC_US);
			result[i] = dataout.read().to_uint();
			eq[i] = compout.read().to_bool();
			cout << "primo numero: " << datain1.read().to_int() << " Secondo numero: " << datain2.read().to_int() << 
			" Operazione numero: " << selop.read().to_uint() << " Risultato operazione: " << dataout.read().to_int() << 
			" risultato comparazione: " << compout.read().to_bool() << endl;
		
		}
		
	}
	
	void init_values(){
		srand(time(NULL));
		for(int i= 0 ; i<SIZE ; i++){
			/* prima operazione add seconda nand terza pass quarta comparazione*/
			sel[i] = i%4; 
			if (i != 3){ //non ho operazione beq
				ing1[i] = rand()%N_MAX;
				ing2[i] = rand()%N_MAX;
			}
			else if ( i == 3 ){
				ing1[i] = rand()%N_MAX;
				ing2[i] = ing1[i];
			}
				
			
		}
		
	}
};

int sc_main( int argc , char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}