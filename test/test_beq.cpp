#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "beq.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const int SIZE=10;
	static const int N_MAX=16384;
	sc_signal<sc_lv<16> > din1, din2;
	sc_signal<sc_logic> eq_res;
	int ing1[SIZE], ing2[SIZE];
	bool res_eq[SIZE];
	Beq eq_test;

	SC_CTOR(TestBench) : eq_test("eq_test"){
		//inizializzazione e thread
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		eq_test.op1(din1);
		eq_test.op2(din2);
		eq_test.is_eq(eq_res);
	}

	
	bool check(){
		bool error = 0;
		for (int i=0; i<SIZE ; i++){
		
			if (~(res_eq[i]) == ((ing1[i]-ing2[i]) or 0)){
				error = 1;
			}
				

		}
		cout << "il valore di error è: " << error << endl;
		return error;
	}


	private:


	void stimulus_thread(){

		for (int i=0 ; i<SIZE ; i++){
			
			din1.write(ing1[i]);
			din2.write(ing2[i]);
			wait(10,SC_US);
			res_eq[i] = eq_res.read().to_bool();
			cout << "val1 = " << din1.read().to_int() << " val2 = " << din2.read().to_int() << " comparatre da : " << eq_res.read().to_bool() << endl;
		}
	
	}


	void init_values(){
		//indici pari del vettore hanno due numeri uguali e quelli dispari hanno due numeri molto pobabilmente diversi
		srand(time(NULL));
		for(int i=0; i<SIZE ; i++){
		
			if(i%2 == 0){
				ing1[i] = rand()%N_MAX;
				ing2[i] = ing1[i];
			}
			else {
				ing1[i] = rand()%N_MAX;
				ing2[i] = rand()%N_MAX;
			}
		}	
		

	}
	
};

int sc_main (int argv, char** argc){

	TestBench test("test");

	sc_start();
	
	return test.check();

}
