#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "adder.hpp"

using namespace std;
using namespace sc_core;

SC_MODULE(TestBench){

	static const unsigned SIZE = 5;
	static const unsigned N_MAX = 16384; // 2^14 per evitare di generare numeri piu grandi di 16bit
	int inj1[SIZE], inj2[SIZE], result[SIZE] ;	
	sc_signal<sc_lv<N_BIT> > injecter1, injecter2;
	sc_signal<sc_lv<N_BIT> > observer;

	Adder add_test;
	
	SC_CTOR(TestBench) : add_test("add_test")
	{
		init_values();
		SC_THREAD(stimulus_thread);
		add_test.op1(injecter1);
		add_test.op2(injecter2);
		add_test.res(observer);
		
	}

	bool check() 
	{
		bool err =0;
		for (int i = 0; i<SIZE ; i++){
			if ( result[i] != inj1[i] + inj2[i] ){
				err = 1;
			}
		}
		if (err == 0){
			cout << "Operazione eseguita con successo!"<< endl;
		}
		else {
			cout<< "Errore calcolo sbagliato" << endl;
		}
		
		return err;

	}

	private:

	void stimulus_thread(){

		for (int i=0; i<SIZE ; i++){
			injecter1.write(inj1[i]); 
			injecter2.write(inj2[i]);
			wait (10,SC_US);
			result[i] = observer.read().to_int();
			cout << injecter1.read().to_int() << " + " << injecter2.read().to_int() << " = " << observer.read().to_int() << endl;
		}
	}

	void init_values(){
		srand (time(NULL));
		for( int i=0 ; i<SIZE ; i++){
			inj1[i] = rand()%N_MAX;
			inj2[i] = rand()%N_MAX;
		}
	
	}
};



int sc_main( int argc, char* argv[]){

	TestBench test("test");

	sc_start();

	return test.check();

}
