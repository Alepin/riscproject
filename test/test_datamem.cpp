#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "datamem.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static unsigned const N_MAX = 100;
	
	sc_signal<sc_lv<N_BIT> > address;
	sc_signal<sc_lv<N_BIT> > datain;
	sc_signal<sc_lv<N_BIT> > dataout;
	sc_signal<sc_logic> wedatamem;
	sc_signal<sc_logic> readmem;
	sc_signal<bool> reset;
	sc_in<bool> clk;
	
	sc_event stim_event;
	
	int testNumber[11];
	
	bool reset_status;
	bool writeOrRead;
	bool error;
	int chekker;
	
	Data_memory mem_test;
	
	SC_CTOR(TestBench) : mem_test("mem_test"){
		init();
		SC_THREAD(stim_thread);
			sensitive << clk.pos();
			dont_initialize();
		
		SC_METHOD(observer_method)
			sensitive << stim_event;
			dont_initialize();
			
		//collegamento porte
		mem_test.address(this -> address);
		mem_test.datain(this -> datain);
		mem_test.dataout(this -> dataout);
		mem_test.wedatamem(this -> wedatamem);
		mem_test.readmem(this -> readmem);
		mem_test.reset(this -> reset);
		mem_test.clk(this -> clk);
	}
		bool check(){
			return error;
		}
		
		//stampa un indirizzo di memoria 
		void printMem( int addr){
			cout << "All'indirizzo: " << addr << " c'è il valore: " << mem_test.getMemValue( addr) << "\t a " << sc_time_stamp() <<  endl;
		}
		
		private: 
		
		void observer_method(){
			
			printMem(address.read().to_uint());
			
			if (writeOrRead){ // scrittura 
				cout << "scrittura!!" << endl;
				if (reset_status){
					cout << "reset "<< endl;
					for (int i =0; i<SIZE_DATA_MEM; i++){
						if (mem_test.getMemValue(i) == 0)
							error = (error or 0);
						else 
							error = (error or 1);
					}
					
				}
				else{
					if (mem_test.getMemValue(address.read().to_uint()) == testNumber[chekker]  )
						error = (error or 0);
					else 
						error = (error or 1);
				}
				cout << endl;
			}
			else{//lettura
				cout << "lettura!!" << endl;
				if (reset_status){//reset
					cout << "reset " << endl;
					if ( dataout.read().to_int() == 0)
						error = (error or 0);
					else 
						error = (error or 1);
					
				}
				else{
					
					if (dataout.read().to_int() == testNumber[chekker])
						error = (error or 0);
					else 
						error = (error or 1);
					
				}
				cout << endl;
			}
			
			
		}
		
		/* faccio 5 scritture e successivamente 5 letture in locazioni casuali della memoria dopo ogni scrittura e 
		ogni lettura effettuo un controllo con l'observer_method poi un reset della memoria e successivamente 
		altre 5 scritture e 5 letture
		*/
		void stim_thread(){
			wait (SC_ZERO_TIME);
			for (int i=0 ; i<11 ; i++){
				writeOrRead = true; // sto effetuando una scritura
				if (i<5){//scrivo in una cella a caso
					wedatamem.write(SC_LOGIC_1);
					readmem.write(SC_LOGIC_0);
					address.write(rand()%SIZE_DATA_MEM);
					datain.write(testNumber[i]) ;
				}
				if ( i==5){//reset
					reset_status = true;
					reset.write(1);
					readmem.write(SC_LOGIC_0);
					wedatamem.write(SC_LOGIC_1);
					address.write(rand()%SIZE_DATA_MEM);
					datain.write(testNumber[i]) ;
				}
				if (i >5){//altre 5 write
					wedatamem.write(SC_LOGIC_1);
					readmem.write(SC_LOGIC_0);
					address.write(rand()%SIZE_DATA_MEM);
					datain.write(testNumber[i]) ;
				}
			
				/*
				invio l'evento di scrittura avvenuta dopo un 1ns in modo da essere sicuro che sia stata
				effettuara la scrittura su su tutti i segnali e sulle memoria dopo di che effattuo il controllo
				tramite il metodo di osservazione
				*/
				stim_event.notify(1,SC_NS);
				
				//aspetto il prossimo clock per effettuare la lettura
				wait();
				
				
				writeOrRead = false;// sto effettuando una lettura
				
				reset.write(0);
				readmem.write(SC_LOGIC_1);
				wedatamem.write(SC_LOGIC_0);
				//invio la notifica 1 ns dopo per essere sicuro che i segnali siano stati scritti
				stim_event.notify(1,SC_NS);
				
				//aspetto il prossimo clock per effettuare la scrittura
				wait();
				reset_status = false;
				chekker ++;
			}
			
		}
		
		
		void init(){
			chekker = 0;
			writeOrRead = true;
			reset_status = false;
			address.write(0);
			datain.write(0);
			wedatamem.write(SC_LOGIC_0);
			readmem.write(SC_LOGIC_0);
			reset.write(0);
			error = 0;
			srand(time(NULL));
			for (int i =0 ; i<11 ; i++ ){
				testNumber[i] = rand()%N_MAX;
				cout << testNumber[i] << endl;
	
			}
		}
		
		
};
	
	int sc_main ( int argc , char ** argv){
		sc_clock clk ("clk" , 100 , SC_NS);
		
		TestBench test("test");
		test.clk(clk);
		cout << "INIZIO TEST!!!" << endl;
		
		sc_start( 2500 , SC_NS);
		
		cout << test.check() << endl;
		
		return test.check();
		
	}