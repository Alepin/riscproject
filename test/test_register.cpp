#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "register.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const int SIZE = 10;
	static const int N_MAX = 16384;
	
	sc_signal <sc_lv<N_BIT> > in;
	sc_signal <sc_lv<N_BIT> > out;
	sc_signal<bool> reset;
	sc_signal<sc_logic> load;
	sc_in <bool> clk;
	
	int  ingresso[SIZE], uscita[SIZE];
	
	Reg reg_test;
	
	SC_CTOR(TestBench) : reg_test("reg_test"){
		init_values();
		SC_THREAD(stimulus_thread);
		 sensitive << clk.pos();
		 dont_initialize();
		 
		//collegamento porte
		reg_test.datain(in);
		reg_test.dataout(out);
		reg_test.load(this -> load);
		reg_test.reset(this -> reset);
		reg_test.clk(this -> clk);
	}
	
	bool check(){
		bool error = 0;
		for (int i =0; i<SIZE ; i++){
			if ( i != 5 && i!=3 && ingresso[i] != uscita[i])
				error = 1;
			if (i ==3 && uscita[i] != 0)
				error = 1;
			if (i==5 && uscita[i] != ingresso[i-1])
				error=1;
		}
		return error;
	}
	
	private :
	void stimulus_thread(){
		for ( int i =0; i<SIZE ; i++){
			if (i!=5 && i!=3){
				load .write(SC_LOGIC_1);
				in.write(ingresso[i]);
			}
			else if (i == 3){
				reset.write(1);
				load .write(SC_LOGIC_1);
				in.write(ingresso[i]);
			}
			else {
				in.write(ingresso[i]);
			}
			
			wait(1,SC_NS);
			
			cout << load << " tempo: "<<sc_time_stamp() << " ingresso: " << in.read().to_int() << " uscita: " << out.read().to_int() << endl;
			
			uscita[i] = out.read().to_int();
			
			load.write(SC_LOGIC_0);
			reset.write(0);
			
			wait();
		}
	}
	
	void init_values(){
		srand(time(NULL));
		for (int i =0 ; i<SIZE ; i++){
			ingresso[i] = rand()%N_MAX;
		}
	}
	
};

int sc_main (int argc , char** argv){
	
	sc_clock clock("clk" , 100 , SC_NS);
	
	TestBench test("test");
	test.clk(clock);
	
	sc_start(1, SC_US);
	
	return test.check();
	
	
}