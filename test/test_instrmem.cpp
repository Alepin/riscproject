#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "instrmem.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	//static unsigned const N_MAX = 100;
	
	sc_signal < sc_lv<N_BIT> > pc;
	sc_signal< sc_lv < N_BIT> > instruction;
	sc_signal< bool> reset;
	sc_in < bool> clk;
	
	sc_event stim_event;
	
	Instruction_memory imem_test;
	
	sc_lv<N_BIT> testNumber[12];
	int chekker;
	bool error;
	
	SC_CTOR(TestBench) : imem_test("imem_test"){
		
		init();
		
		SC_THREAD(stim_thread);
			sensitive << clk.pos();
			dont_initialize();
		
		SC_METHOD(obs_method)
			sensitive << stim_event;
			dont_initialize();
			
		//collegamento porte	
		imem_test.instruction(this -> instruction);
		imem_test.pc (this -> pc);
		imem_test.reset (this -> reset);
		imem_test.clk (this -> clk);
	}
	
	bool check(){
		return error;
	}
	
	void printIstruction( int pcadd){
		cout << "pc vale: " << chekker  << " al tempo: " << sc_time_stamp() << "\t l'uscita vale: " << instruction << "\t l'istruzione vale: " <<  imem_test.getInstr(pcadd) << endl;
	}
	
	private:
	
	void obs_method(){
		//print di solo le prime 20 istruzioni
		if ( chekker < 12 ){
			printIstruction(chekker);
			if ( instruction.read() == testNumber[chekker] )
				error = error or 0;
			else 
				cout << "qui al ciclo " << chekker << endl;//error = error or 1;
		}
		else if (chekker < 20){
			printIstruction(chekker);
			if ( instruction.read() == 0)
				error = error or 0;
			else 
				 error = error or 1;
		}
		else {
			if ( instruction.read() == 0)
				error = error or 0;
			else 
				 error = error or 1;
		}
		
	}
	
	void stim_thread(){
		
		for( int i=0; i<SIZE_INSTR_MEM/2; i++){
			
			pc.write(i);
			
			stim_event.notify( 1 , SC_NS);
			
			
			wait();
			chekker++;
		}
		
	}
	
	void init(){
		
		chekker = 0;
		error = 0;
		pc.write(0);
		reset.write(0);
		for ( int i = 0; i<12; i++){
			switch (i){
				case 0:{//prima istruzione
					testNumber[i] = "0110100000011001";
					break;
				}
				case 1: {//seconda istruzione 
				testNumber[i] = "0010010100110010";
				break;
			}
			case 2:{//terza istruzione
				testNumber[i] = "0100110100000001";
				break;
			}
			case 3:{//quarta istruzione
				testNumber[i] = "0001000100000001";
				break;
			}
			case 4: {// quinta istruzione
				testNumber[i] = "1010010110000000";
				break;
			}
			case 5: {// sesta istruzione 
				testNumber[i] = "1010100110000001";
				break;
			}
			case 6:{//settima istruzione
				testNumber[i] = "1101011000000100";
				break;
			}
			case 7:{//ottava istruzione 
				testNumber[i] ="1001010110000001";
				break;
			}
			case 8:{// nona istruzione 
				testNumber[i] ="0001010010000101";
				break;
			}
			case 9:{//decima istruzione
							
				testNumber[i] ="0111110000000110";
				break;
			}
			case 10: {// Undicesima istruzio
							
				testNumber[i] ="1111101110000000";
				break;
			}
			case 11:{//dodicesima istruzione
				testNumber[i] ="0011100000100001";
				break;
			}
			}
		}
	}
	
};

int sc_main( int argc , char ** argv){
	
	sc_clock clk ("clk" , 100 , SC_NS);
	
	TestBench test("test");
	
	test.clk(clk);
	
	cout << " INIZIO TEST!!!!"<< endl;
	
	sc_start ( 25700 , SC_NS);
	
	cout << test.check() << endl;
	
	return test.check();
	
	
}