#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "signext10.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static unsigned const MAX = 1024;
	
	sc_signal<sc_lv<BIT_TEN> > in10;
	sc_signal<sc_lv< N_BIT> > signext10;
	
	bool error;
	int stimVec[10];
	int chekker;
	sc_event stim_event;
	
	Sign_extend10 signext10_test;
	
	SC_CTOR(TestBench) : signext10_test("signext10_test"){
		init();
		
		SC_THREAD(stim_thread);
		
		SC_METHOD(obs_method);
			sensitive << stim_event;
			dont_initialize();
		
		//collegamento porte
		signext10_test.in10(this -> in10);
		signext10_test.signext10 (this -> signext10);
		
	}
	
	bool check(){
		return error;
	}
	
	private:
	void obs_method(){
		
		cout << "ingresso vale " << in10.read() << "\t uscita vale: " << signext10.read() << "\t al tempo: " << sc_time_stamp() << endl;
		
		if ( in10.read().to_int() == signext10.read().to_int())
			error = error or 0;
		else 
			error = error or 1;
		
	}
	
	void stim_thread(){
		for ( int i = 0; i<10 ; i++){
			
			in10.write(stimVec[i]);
			wait(SC_ZERO_TIME);
			stim_event.notify(SC_ZERO_TIME);
			wait (100, SC_NS);
			
		}
	}
	
	void init(){
		chekker = 0;
		error = 0;
		srand(time(NULL));
		for (int i=0 ; i<10 ; i++){
			stimVec[i] = rand()%MAX;
		}
	}
	
};

int sc_main ( int argc, char** argv){
	
	TestBench test("test");
	
	cout << "INIZIO TEST!!!" << endl;
	
	sc_start();
	
	return test.check();
	
}