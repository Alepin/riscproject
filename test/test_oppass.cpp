#include <systemc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "oppass.hpp"

using namespace std;

SC_MODULE(TestBench){
	
	static const int SIZE=10;
	static const int N_MAX=16384;
	
	sc_signal<sc_lv<N_BIT> > in1, in2, result;
	int ing1[SIZE],  ing2[SIZE], out[SIZE];
	
	Op_pass pass_test;
	
	SC_CTOR(TestBench) : pass_test("pass_test"){
		//inizializzazione e thread di stimolo
		init_values();
		SC_THREAD(stimulus_thread);
		//collegamento porte
		pass_test.op1(in1);
		pass_test.op2(in2);
		pass_test.res(result);
	}
	
	bool check(){
		bool error = 0;
		for (int i = 0 ; i<SIZE ; i++){
			if (out[i] != ing1[i])
				error = 1;
		}
		if (error == 0)
			cout << "successo!" << endl;
		else 
			cout << "errore!" << endl;
		return error;
	}
	
	private:
	
	void stimulus_thread(){
		for (int i =0; i<SIZE ; i++){
			in1.write(ing1[i]);
			in2.write(ing2[i]);
			wait (10,SC_US);
			out[i] = result.read().to_int();
			cout << "Primo numero: " << in1.read().to_int() << " Secondo numero: " << in2.read().to_int() << " Risultato: " << result.read().to_int() << endl;
		}
	}
	
	void init_values(){
		srand(time(NULL));
		for (int i =0; i<SIZE ; i++){
			ing1[i] = rand()%N_MAX;
			ing2[i] = rand()%N_MAX;
		}
		
	}
	
};

int sc_main(int argc, char** argv){
	
	TestBench test("test");
	
	sc_start();
	
	return test.check();
	
}